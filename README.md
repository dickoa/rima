
<!-- README.md is generated from README.Rmd. Please edit that file -->

# rima an R package to conduct Resilience Measurement Index Analysis

<!-- badges: start -->

<!-- badges: end -->

The goal of rima is to …

## Installation

You can install the development version of `rima` from
[Gitlab](https://gitlab.com/dickoa/rima) with:

``` r
# install.package("remotes")
remotes::install_gitlab("dickoa/rima")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(rima)
## basic example code
```
